import React, { Component } from 'react';

// Components
import { Text } from 'react-native';
import BaseView from '../common/baseView';
import DrawerIcon from '../common/drawerIcon';

export default class SettingsView extends Component {
  static navigationOptions = {
    drawerLabel: 'Settings',
    drawerIcon: options => <DrawerIcon ios='ios-settings' android='md-settings' options={options} />
  };

  render() {
    return (
      <BaseView navigation={this.props.navigation} title='Settings'>
        <Text>Settings</Text>
      </BaseView>
    );
  }
}
