import React, { Component } from 'react';

// Controller
import FilesController from '../../controllers/filesController';

// Components
import BaseView from '../common/baseView';
import DrawerIcon from '../common/drawerIcon';
import FileExplorer from '../common/fileExplorer';

export default class FilesView extends Component {

  static navigationOptions = {
    drawerLabel: 'Files',
    drawerIcon: options => <DrawerIcon ios='ios-folder' android='md-folder' options={options}/>
  };

  controller = new FilesController(this);

  state = {
    currentPath: this.controller.rootPath,
    searchQuery: '',
    dirItems: [],
    isLoading: false
  };

  componentWillMount = this.controller.viewWillMount;

  render() {
    return (
      <BaseView navigation={this.props.navigation} title='Files'>
        <FileExplorer
          currentPath={this.state.currentPath}
          searchQuery={this.state.searchQuery}
          dirItems={this.state.dirItems}
          isLoading={this.state.isLoading}

          isAtRoot={this.controller.isAtRoot()}
          addActionSheetOptions={this.controller.addActionSheetOptions}

          goToDir={this.controller.goToDir}
          goUpDir={this.controller.goUpDir}
          onSearchChanged={this.controller.onSearchChanged}
          onActionSheetSelect={this.controller.onActionSheetSelect}
        />
      </BaseView>
    );
  }

}
