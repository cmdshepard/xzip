import React, { Component, PropTypes } from 'react';

// Components
import {
  Header,
  Left,
  Icon,
  Body,
  Title,
  Right,
  Container,
  Content,
  Button
} from 'native-base';

// Constants
import Colors from '../../constants/colors';

export default class BaseView extends Component {
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              title='Menu'
              onPress={() => this.props.navigation.navigate('DrawerOpen')}
            >
              <Icon
                style={{color: Colors.PRIMARY}}
                ios='ios-menu'
                android='md-menu'
              />
            </Button>
          </Left>

          <Body>
            <Title>{this.props.title}</Title>
          </Body>

          <Right />
        </Header>

        <Content>
          {this.props.children}
        </Content>
      </Container>
    );
  }

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired
  };
}