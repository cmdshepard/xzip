import React, { Component, PropTypes } from 'react';

// Constants
import Colors from '../../constants/colors';

// Libraries
import prettysize from 'prettysize';
import moment from 'moment';

// Components
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';

export default class DirectoryItem extends Component {
  style = {
    container: {
      flex: 1,
      flexDirection: 'row',
      paddingTop: 10,
      paddingBottom: 10,
      borderTopColor: Colors.LIGHT_GREY,
      borderTopWidth: 0.5,
      borderBottomColor: Colors.LIGHT_GREY,
      borderBottomWidth: 0.5,
    },
    iconView: {
      flex: 0.15,
      justifyContent: 'center'
    },
    icon: {
      color: Colors.DARKER_GREY,
      textAlign: 'center',
      fontSize: 25
    },
    detailsView: {
      flex: 0.85,
      justifyContent: 'center'
    },
    title: {
      color: Colors.DARKER_GREY,
      fontSize: 14
    },
    details: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingTop: 1
    },
    detailsText: {
      color: Colors.DARK_GREY,
      fontSize: 9
    },
    size: {
      color: Colors.DARK_GREY,
      paddingRight: 10
    }
  };

  isDir = this.props.details.isDirectory();

  get icon() {
    return {
      ios: this.isDir ? 'ios-folder' : 'ios-document',
      android: this.isDir ? 'md-folder' : 'md-document'
    };
  }

  render() {
    console.log(this.props.details);
    return (
      <TouchableOpacity
        style={this.style.container}
        onPress={
          () => this.isDir
            ?
            this.props.goToDir(this.props.details.name)
            :
            console.log('===== its a file')
        }
      >
          <View style={this.style.iconView}>
            <Icon
              style={this.style.icon}
              ios={this.icon.ios}
              android={this.icon.android}
            />
          </View>

          <View style={this.style.detailsView}>
            <Text style={this.style.title}>{this.props.details.name} </Text>

            <View style={this.style.details}>
              <Text style={this.style.detailsText}>
                {moment(this.props.details.mtime).format('H:mm - MMM Mo YYYY')}
              </Text>

              <Text style={[this.style.detailsText, this.style.size]}>
                {prettysize(this.props.details.size, false, true)}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
    );
  }

  static propTypes = {
    goToDir: PropTypes.func.isRequired,
    details: PropTypes.object.isRequired
  };

}
