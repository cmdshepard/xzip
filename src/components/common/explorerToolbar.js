import React, { Component, PropTypes } from 'react';

// Constants
import Colors from '../../constants/colors';

// Components
import { View } from 'react-native';
import { Button, Icon, Item, Input, ActionSheet } from 'native-base';

export default class ExplorerToolbar extends Component {
  style = {
    container: {
      borderBottomColor: Colors.LIGHT_GREY,
      borderBottomWidth: 0.5,
      padding: 10,
      flexDirection: 'row'
    },
    button: {
      width: 50,
      height: 50,
      padding: 0
    },
    icon: {
      fontSize: 26
    }
  };

  _showActionSheet = () => ActionSheet
    .show(
      this.props.addActionSheetOptions,
      i => this.props.onActionSheetSelect(i)
    );

  render() {
    return (
      <View style={this.style.container}>
        { !this.props.isAtRoot &&
          <Button transparent title='Back' onPress={this.props.goUpDir}>
            <Icon ios='ios-arrow-back' android='md-arrow-back' style={this.style.icon} />
          </Button>
        }

        <Item rounded style={{flex: 1}} label='Search'>
          <Input
            placeholder='Search'
            value={this.props.searchQuery}
            onChangeText={this.props.onSearchChanged}
          />
        </Item>

        <Button transparent title='Add' onPress={this._showActionSheet}>
          <Icon name='md-add' style={this.style.icon} />
        </Button>
      </View>
    );
  }

  static propTypes = {
    searchQuery: PropTypes.string.isRequired,
    onSearchChanged: PropTypes.func.isRequired,
    goUpDir: PropTypes.func.isRequired,
    addActionSheetOptions: PropTypes.object.isRequired,
    onActionSheetSelect: PropTypes.func.isRequired,
    isAtRoot: PropTypes.bool.isRequired
  };
}
