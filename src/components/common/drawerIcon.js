import React, {
  Component,
  PropTypes
} from 'react';

// Components
import { Icon } from 'native-base';

export default class DrawerIcon extends Component {
  render() {
    const options = this.props.options;

    return <Icon
      ios={this.props.ios}
      android={this.props.android}
      style={{color: options.tintColor}}
    />
  }

  static propTypes = {
    ios: PropTypes.string.isRequired,
    android: PropTypes.string.isRequired,
    options: PropTypes.object.isRequired
  };
}
