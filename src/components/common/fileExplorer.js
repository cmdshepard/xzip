import React, { Component, PropTypes } from 'react';

// Constants
import Colors from '../../constants/colors';

// Components
import { View, Text } from 'react-native';
import { Spinner } from 'native-base';
import ExplorerToolbar from './explorerToolbar';
import DirectoryItem from './directoryItem';

export default class FileExplorer extends Component {
  style = {
    emptyText: {
      paddingTop: 30,
      textAlign: 'center',
      color: Colors.LIGHT_GREY,
      fontSize: 30
    }
  };

  render() {
    return (
      <View>
        <ExplorerToolbar
          searchQuery={this.props.searchQuery}
          addActionSheetOptions={this.props.addActionSheetOptions}
          isAtRoot={this.props.isAtRoot}

          goUpDir={this.props.goUpDir}
          onSearchChanged={this.props.onSearchChanged}
          onActionSheetSelect={this.props.onActionSheetSelect}
        />

        {this.props.isLoading
          ?
          <Spinner color={Colors.PRIMARY}/>
          :
          this._renderDirectory()
        }
      </View>
    );
  }

  _renderDirectory() {
    return (
      <View>
        {
          this.props.dirItems.length > 0
          ?
          this.props.dirItems
            .map((item, i) => this.props.searchQuery === ''
              ?
              <DirectoryItem key={i} details={item} goToDir={this.props.goToDir}/>
              :
              (item.name.search(new RegExp(this.props.searchQuery, 'gi')) !== -1)
                && <DirectoryItem key={i} details={item} goToDir={this.props.goToDir}/>
            )
          :
            <Text style={this.style.emptyText}>No Files Found</Text>
        }
      </View>
    )
  }

  static propTypes = {
    currentPath: PropTypes.string.isRequired,
    searchQuery: PropTypes.string.isRequired,
    addActionSheetOptions: PropTypes.object.isRequired,
    dirItems: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isAtRoot: PropTypes.bool.isRequired,

    goToDir: PropTypes.func.isRequired,
    goUpDir: PropTypes.func.isRequired,
    onActionSheetSelect: PropTypes.func.isRequired,
    onSearchChanged: PropTypes.func.isRequired
  };
}
