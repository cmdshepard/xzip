export default class BaseController {

  /** @param {React.Component} viewComponent */
  constructor(viewComponent) {
    this.view = viewComponent;
  }

}
