import Controller from './baseController';

// Libraries
import { AsyncStorage, Platform } from 'react-native';
import { zip, unzip, unzipAssets, subscribe } from 'react-native-zip-archive';
import FileSystem, { MainBundlePath, DocumentDirectoryPath } from 'react-native-fs';
import Prompt from 'react-native-prompt-android';

export default class FilesController extends Controller {

  /** @type {boolean} */
  isiOS = Platform.OS === 'ios';

  /** @type {string} */
  rootPath = {
    ios: `${MainBundlePath}/xzip_docs/`,
    android: `${DocumentDirectoryPath}/xzip_docs/`
  }[Platform.OS];

  tempPath = {
    ios: MainBundlePath,
    android: DocumentDirectoryPath
  }[Platform.OS];

  restoreZipPath = `${this.tempPath}/xzip_docs_restored`;

  /** @returns {boolean} */
  isAtRoot = () => this.rootPath === this.view.state.currentPath;

  /** View's componentWillMount method */
  viewWillMount = () => {
    this.view.setState({isLoading: true});

    FileSystem
      .readDir(this.isiOS ? MainBundlePath : DocumentDirectoryPath)
      .then(dir => this.setupHomeDir(dir))
      .then(() => this.refreshCurrentDir(this.rootPath));
  };

  /**
   * Sets up the home directory
   * @param {array} dir - Directory array from FileSystem.readDir
   */
  setupHomeDir = dir => new Promise(resolve => {
    // Try and find the home directory
    const homeDir = dir.reduce((acc, obj) => {
      const isDir = obj.isDirectory();
      if (isDir && obj.name === 'xzip_docs') { acc = obj; }
      return acc;
    }, null);

    if (!homeDir) {
      const createHomeDir = FileSystem.mkdir(this.rootPath);

      AsyncStorage
        .getItem('xzip_docs')
        .then(compressedBytes => createHomeDir.then(() => compressedBytes))
        .then(compressedBytes => compressedBytes
          ?
          FileSystem
            .writeFile(this.restoreZipPath, compressedBytes, 'base64')
            .then(() => unzip(this.restoreZipPath, this.rootPath))
            .then(() => FileSystem.unlink(this.restoreZipPath))
            .then(() => FileSystem.readDir(this.rootPath))
            .then(dirItems => resolve(dirItems))
          :
          resolve([])
        )
        .catch(() => createHomeDir.then(() => resolve([])));
    } else {
      // Directory already exists
      resolve(dir);
    }
  });

  /** Persist local home directory to local storage */
  persistFiles = () => zip(this.rootPath, this.restoreZipPath)
    .then(() => FileSystem.readFile(this.restoreZipPath, 'base64'))
    .then(compressedBytes => AsyncStorage.setItem('xzip_docs', compressedBytes))
    .then(() => FileSystem.unlink(this.restoreZipPath));

  /**
   * Handles search text change
   * @param {string} searchQuery
   */
  onSearchChanged = searchQuery => this.view.setState({searchQuery});

  /**
   * Refreshes the current directory or path if provided
   * @param {?string} path
   * */
  refreshCurrentDir = path => this.goToPath(path || this.view.state.currentPath);

  /**
   * Change directory to to the absolute path provided
   * @param {string} path
   */
  goToPath = path => this.view.setState(
    {isLoading: true},
    () => FileSystem
      .readDir(path)
      .then(dirItems => this.view.setState(
        {dirItems, currentPath: path},
        () => this.view.setState({isLoading: false})
      ))
  );

  /**
   * Changes directory from current directory to directory specified
   * @param {string} dirName
   */
  goToDir = dirName => this.goToPath(`${this.view.state.currentPath}${dirName}/`);

  /** Go up the directory tree by one */
  goUpDir = () => {
    if (!this.isAtRoot()) {
      const oldPathArr = this.view.state.currentPath.split('/');
      oldPathArr.splice(oldPathArr.length - 2, 1);

      const newPath = oldPathArr.join('/');
      this.goToPath(newPath);
    }
  };

  /** @type {object} */
  addActionSheetOptions = {
    options: [
      'New Folder',
      'Import From iCloud Drive',
      'Import From Dropbox',
      'Import From Box',
      'Import From Google Drive',
      'Import From Photos',
      'Cancel'
    ],
    cancelButtonIndex: 6
  };

  /**
   * Callback for add button
   * @param {number} i - Index of selection that has been tapped
   */
  onActionSheetSelect = i => [
    this.onCreateNewFolder,
    () => {},
    () => {},
    () => {},
    () => {},
    () => {},
    () => {}
  ][i]();

  /** Callback that gets called when user taps New Folder from action sheet */
  onCreateNewFolder = () => new Promise((resolve, reject) => {
    Prompt(
      'New Folder',
      'Enter the name of your new folder',
      [
        {text: 'Cancel', onPress: () => reject(), style: 'cancel'},
        {text: 'Create', onPress: newFolderName => !!newFolderName && newFolderName !== '' ? resolve(newFolderName) : reject()},
      ],
      'plain-text'
    );
  })
  .then(newFolderName => FileSystem
    .mkdir(`${this.view.state.currentPath}${newFolderName}/`)
    .then(() => this.persistFiles())
    .then(() => this.refreshCurrentDir())
  )
  .catch(() => console.log('Create new folder prompt cancelled'));

}
