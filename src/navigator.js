import React from 'react';
import { DrawerNavigator, DrawerItems } from 'react-navigation';
import { ScrollView } from 'react-native';

// Constants
import Colors from './constants/colors';

// Views
import FilesView from './components/views/filesView';
import SettingsView from './components/views/settingsView';

export default DrawerNavigator(
  // Routes
  {
    files: {screen: FilesView},
    settings: {screen: SettingsView}
  },

  // Drawer Configurations
  {
    contentComponent: props => (
      <ScrollView>
        <DrawerItems {...props} />
      </ScrollView>
    ),

    contentOptions: {
      activeTintColor: Colors.PRIMARY,
      inactiveTintColor: Colors.BLACK
    }
  }
);
